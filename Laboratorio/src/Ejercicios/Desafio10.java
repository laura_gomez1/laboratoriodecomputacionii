package Ejercicios;
import java.util.Scanner;

public class Desafio10 {

	public static void main(String [] args) {
		
		Scanner sc = new Scanner(System.in);
		int intentos = 0;
		int numeroRandom = (int) (Math.random() * 100);
		int ingresado;
		
		do {
			System.out.println("Ingrese un numero entre 1 y 100");
			ingresado = sc.nextInt();
			
			if(ingresado > numeroRandom) {
					
				System.out.println("El n�mero random es menor");
				
			} else if (ingresado < numeroRandom){
				
				System.out.println("El n�mero random es mayor");
				
			}
			intentos = intentos + 1;
			
		}while(ingresado != numeroRandom);
		
		if (ingresado == numeroRandom) {

				System.out.println("Correcto!");	
		}
		
		System.out.println("El numero elegido es: " + numeroRandom);
		System.out.println("Intentos: " + intentos);
   }
}