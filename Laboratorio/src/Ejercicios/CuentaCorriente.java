package Ejercicios;

public class CuentaCorriente {

	private double saldo;
	private String nombreTitular;
	private long numeroCuenta;
	
	public CuentaCorriente(double saldo, String nombreTitular) {
		// TODO Auto-generated constructor stub
		this.setSaldo(saldo);
		this.setNombreTitular(nombreTitular);
		this.numeroCuenta = (int) (Math.random() * 50);
	}


	private void setNombreTitular(String nombreTitular2) {
		// TODO Auto-generated method stub
		nombreTitular = nombreTitular2;
	}


	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo += saldo;
	}
	
	public void Transferir(CuentaCorriente c2, double importe) {
               this.saldo -= importe;
               c2.saldo += importe;
	}


	public String getDatosGenerales() {
		return "Titular " + nombreTitular + " Nro Cuenta " + numeroCuenta + " Saldo " + saldo;
	}

		
}
