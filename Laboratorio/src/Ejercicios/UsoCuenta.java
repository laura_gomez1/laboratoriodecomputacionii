package Ejercicios;

public class UsoCuenta {
	
	public static void main (String [] args) {
		
		CuentaCorriente cuenta1 = new CuentaCorriente(3500, "Maria");
		CuentaCorriente cuenta2 = new CuentaCorriente(5500, "Jos�");
		
		cuenta1.Transferir(cuenta2, 500);
		
		System.out.println("Saldo de la cuenta 1 " +cuenta1.getSaldo());
		System.out.println("Datos generales de la cuenta \n" +cuenta1.getDatosGenerales());
	}	

}
