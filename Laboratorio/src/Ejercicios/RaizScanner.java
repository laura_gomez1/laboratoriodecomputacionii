package Ejercicios;

import java.util.Scanner;

public class RaizScanner {

	public static void main(String[] args) {
     	
	       Scanner sc = new Scanner(System.in);
	       
	       System.out.println("Ingrese el numero para calcular la raiz cuadrada: ");
	       
	       int numero = sc.nextInt();
	       
	       double raiz = Math.sqrt(numero);
	       
	       System.out.println(raiz);
	}
}
