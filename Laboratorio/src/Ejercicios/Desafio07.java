package Ejercicios;
import java.util.Scanner;

public class Desafio07 {

	public static void main(String[] args) {
	     	
	       Scanner sc = new Scanner(System.in);
	       
	       System.out.println("Ingrese el numero para calcular: ");
	       
	       int numero = sc.nextInt();
	       
	       double seno = Math.sin(numero);

	       double coseno = Math.cos(numero);

	       double tangente = Math.tan(numero);

	       double cotangente = Math.atan(numero);

	       double angulo = Math.atan2(numero, 4);
	       
	       double exponente = Math.exp(numero);
	       
	       double logaritmo = Math.log(numero);
	       
	       double pi = Math.PI;
	       
	       double E = Math.E;
	    		   
	       System.out.println("El seno de " + numero + " es: " + seno);
	       System.out.println("El coseno de " + numero + " es: " + coseno);
	       System.out.println("La tangente de " + numero + " es: " + tangente);
	       System.out.println("La cotangente de " + numero + " es: " + cotangente);
	       System.out.println("El angulo de " + numero + " es: " + angulo);
	       System.out.println("El exponente de " + numero + " es: " + exponente);
	       System.out.println("El logaritmo de " + numero + " es: " + logaritmo);
	       
	       
	}
}
