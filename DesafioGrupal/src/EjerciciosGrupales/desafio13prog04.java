package EjerciciosGrupales;
import java.util.Arrays;

public class desafio13prog04 {

public static void main (String [] args) {
		
		System.out.println("\nMatriz original \n");
		cargarMatriz();
		
	}

	private static void imprimirMatriz(int [][] m) {
		// TODO Imprime la matriz recibida como parámetro
		 for (int i = 0; i < m.length; i++) {
				for (int j = 0; j < m.length; j++) {
					System.out.print(m[i][j] + " ") ;  
				}

				System.out.println();
			}

		
	}

	private static void ordenarMatriz(int [][] m) {
		// TODO Ordeno la matriz 
		 int t = 0;
		    for (int i = 0; i < m.length; i++) {
				for (int j = 0; j < m.length; j++) {
					for (int x = 0; x < m.length; x++) {
						for (int y = 0; y < m.length; y++) {
							if(m[i][j] < m[x][y]){
								t = m[i][j];
								m[i][j] = m[x][y];
								m[x][y] = t;
								}
						}
					}
				}
			}
		 System.out.println("\nMatriz ordenada \n");   
        imprimirMatriz(m);  
		
	}

	private static void cargarMatriz() {
		// Cargo la matriz con valores random hasta el 10
		int [][] matrizRandom = new int [3][3];
		
	    for (int i = 0; i < matrizRandom.length; i++) {
			for (int j = 0; j < matrizRandom.length; j++) {
				matrizRandom[i][j] = (int) (Math.random() * 10);  
			}
		}
	    imprimirMatriz(matrizRandom);
		ordenarMatriz(matrizRandom);

	}
	
}
