package ElBanco;

import java.io.Serializable;

public class CuentaCorriente extends Operaciones implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String nombreTitular;
	private int nro_cuenta;
	private double saldo;
	
	public CuentaCorriente(String nombreTitular, int nro_cuenta, double saldo) {
		this.nombreTitular = nombreTitular;
		this.nro_cuenta = nro_cuenta;
		this.saldo = saldo;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public int getNro_cuenta() {
		return nro_cuenta;
	}

	public void setNro_cuenta(int nro_cuenta) {
		this.nro_cuenta = nro_cuenta;
	}

	public double getSaldo() {
		return saldo;
	}

	public void ingresarSaldo(double saldo) {
		this.saldo += saldo;
	}

	public void egresarSaldo(double saldo) {
		if (this.getSaldo()>= saldo) {
			this.saldo -= saldo;
		}
		else System.out.println("No tiene saldo en la cuenta. Su saldo es de: " + this.getSaldo());
	}


	public String toString() {
		return "CuentaCorriente [nombreTitular=" + nombreTitular + ", nro_cuenta=" + nro_cuenta + ", saldo=" + saldo
				+ "]";
	}

	@Override
	public void transferencia(CuentaCorriente cuenta, double importe) {
		// TODO Auto-generated method stub
		if (this.getSaldo()>= importe) {
			this.egresarSaldo(importe);
			cuenta.ingresarSaldo(importe);
		}
		else System.out.println("No tiene saldo en la cuenta. Su saldo es de: " + this.getSaldo());
	}

}
	

