package ElBanco;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Banco {

	public static void main(String[] args) {
       
		CuentaCorriente cuenta = new CuentaCorriente("Laura", 12345, 5000);

		CuentaCorriente cuenta2 = new CuentaCorriente("Kerem", 98765, 2000);
		
		Set<CuentaCorriente> listaCuentas = new HashSet<>();
		listaCuentas.add(cuenta);
		listaCuentas.add(cuenta2);
		
		System.out.println("Los datos de las cuentas ingresadas son: "+listaCuentas.toString());

        cuenta.transferencia(cuenta2, 300.00);
        System.out.println("");
        System.out.println("Los datos de las cuentas luego de la transferencia son: "+listaCuentas.toString());
        
        
        try {
            ObjectOutputStream escriboDatos = new ObjectOutputStream(new FileOutputStream(
                    "C:\\Users\\lgomez\\Documents\\Perso\\Laboratorioii\\laboratoriodecomputacionii\\ElBanco\\listaCuentas.dat"));

            escriboDatos.writeObject(listaCuentas);
            escriboDatos.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {

            ObjectInputStream leoDatos = new ObjectInputStream(new FileInputStream(
                    "C:\\Users\\lgomez\\Documents\\Perso\\Laboratorioii\\laboratoriodecomputacionii\\ElBanco\\listaCuentas.dat"));

            Set<CuentaCorriente> listaDeCuentas = (Set<CuentaCorriente>) leoDatos.readObject();
            System.out.println("");
            System.out.println("Las cuentas guardadas en el archivo son: "+ listaDeCuentas.toString());

            leoDatos.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        
           }
        }		
		
}

