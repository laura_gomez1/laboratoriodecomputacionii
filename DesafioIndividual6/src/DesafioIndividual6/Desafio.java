package DesafioIndividual6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

public class Desafio extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Desafio frame = new Desafio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Desafio() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 596, 338);
		contentPane = new JPanel();
		contentPane.setBackground(Color.MAGENTA);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JButton btnNewButton = new JButton("Bot\u00F3n");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				btnNewButton.addMouseListener(new MouseAdapter() {
			         public void mouseEntered(MouseEvent evt) {
			            Color c = btnNewButton.getBackground(); 
			            btnNewButton.setBackground(btnNewButton.getForeground());
			            btnNewButton.setForeground(c);
			            
			         }
			         public void mouseExited(MouseEvent evt) {
			            Color c = btnNewButton.getBackground();
			            //btnNewButton.setBackground(btnNewButton.getForeground());
			            btnNewButton.setForeground(c);
			         }
			      });


			}
		});
		contentPane.add(btnNewButton, BorderLayout.EAST);
		
		JButton btnNewButton_1 = new JButton("Otro bot\u00F3n");
		btnNewButton_1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				btnNewButton_1.addMouseListener(new MouseAdapter() {
			         public void mouseEntered(MouseEvent evt) {
			            Color c = btnNewButton_1.getBackground(); 
			            btnNewButton_1.setBackground(btnNewButton_1.getForeground());
			            btnNewButton_1.setForeground(c);
			            
			         }
			         public void mouseExited(MouseEvent evt) {
			            Color c = btnNewButton_1.getBackground();
			            //btnNewButton.setBackground(btnNewButton.getForeground());
			            btnNewButton_1.setForeground(c);
			         }
			      });
			}
		});
		contentPane.add(btnNewButton_1, BorderLayout.WEST);
	}

}
