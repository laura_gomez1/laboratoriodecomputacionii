package TtrabajoIndividialProcesadorDeTexto;
import java.awt.event.*;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.awt.*;
import javax.swing.*;
import javax.swing.text.*;


public class Procesador {

	public static void main(String[] args) {
		
		MenuProcesador marco = new MenuProcesador();
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		marco.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		
		marco.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
			      
				if (JOptionPane.showConfirmDialog(null, "Est� seguro de salir?", "Exit",
				        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				         
					    try{
					       
 
				            if (LaminaProcesador.areatexto.getText().length() != 0) {
				            	
				            	FileWriter fw=new FileWriter("C:\\Users\\lgomez\\Documents\\Perso\\Laboratorioii\\autoguardado.txt");
				                fw.write(LaminaProcesador.areatexto.getText());
								JOptionPane.showMessageDialog(null, "El archivo se autoguardar�");
								fw.close(); 
				            	
				            } else JOptionPane.showMessageDialog(null, "Archivo vac�o, se cerrar� el procesador.");
				            
				            
				      
				        }catch(IOException e1){
				        	JOptionPane.showMessageDialog(null, "Se ha producido un error en el guardado. "+e1);
				        }

					    System.exit(0); 
				} 
			}
		});

	
    }	
}

class MenuProcesador extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MenuProcesador() {
		
		setBounds(500, 400, 500, 400);
		LaminaProcesador lamina = new LaminaProcesador();
		add(lamina);
		setTitle("Procesador de Texto");
		setVisible(true);
		
	}
		
}

class LaminaProcesador extends JPanel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	static JTextPane areatexto;
	JMenu archivo, fuente, estilo, tamanio;
	Font letras;
	
	
	public LaminaProcesador() {

		
		setLayout(new BorderLayout());
		
		JPanel laminamenu = new JPanel();
		
		JMenuBar mibarra = new JMenuBar();
		
	//Elementos Menu
		fuente = new JMenu("Fuente");
		estilo = new JMenu("Estilo");
		tamanio = new JMenu("Tama�o");
		archivo = new JMenu("Archivo");
		
	//Elementos item	
		configura_menu("Abrir","archivo","",1,1);
		configura_menu("Guardar","archivo","",1,1);
		
		configura_menu("Arial","fuente","Arial",9,10);
		configura_menu("Courier","fuente","Courier",9,10);
		configura_menu("Verdana","fuente","Verdana",9,10);

		configura_menu("Negrita","estilo","",Font.BOLD,1);
		configura_menu("Cursiva","estilo","",Font.ITALIC,1);
		
		configura_menu("12","tamanio","",9,12);
		configura_menu("14","tamanio","",9,14);
		configura_menu("16","tamanio","",9,16);
		configura_menu("20","tamanio","",9,20);
		configura_menu("24","tamanio","",9,24);
		
   //Elementos mibarra	
		mibarra.add(archivo);
		mibarra.add(fuente);
		mibarra.add(estilo);
		mibarra.add(tamanio);
		
		laminamenu.add(mibarra);
		add(laminamenu,BorderLayout.NORTH);
		
		areatexto = new JTextPane();
		add(areatexto,BorderLayout.CENTER);
        JScrollPane panelConScroll = new JScrollPane(areatexto);
        add(panelConScroll);

		
	}
	
	public void configura_menu(String rotulo, String menu, String tipo_letra, int estilos, int tam) {
		
		JMenuItem elemento = new JMenuItem(rotulo);
		
		if(menu == "archivo") {
			
			archivo.add(elemento);	
			
			if (rotulo == "Abrir") {
				elemento.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						abrir();
					}
				});
			} else if (rotulo == "Guardar") {
				elemento.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						guardar();
					}
				});
			}
			
			
		} else if(menu == "fuente") {
			
			fuente.add(elemento);
			
			if (tipo_letra == "Arial") {
				
				elemento.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra", "Arial"));
				
			} else if (tipo_letra == "Courier") {
				
				elemento.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra", "Courier"));
				
			}else if (tipo_letra == "Verdana") {
				
				elemento.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra", "Verdana"));
			}		
			
		} else if (menu == "estilo") {
			
			estilo.add(elemento);
			
			if(estilos == Font.ITALIC) {
				
				elemento.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K,InputEvent.CTRL_DOWN_MASK));
				
				elemento.addActionListener(new StyledEditorKit.ItalicAction());
				
			}else if (estilos == Font.BOLD)
				
				elemento.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,InputEvent.CTRL_DOWN_MASK));
				
				elemento.addActionListener(new StyledEditorKit.BoldAction());
			
		} else if (menu == "tamanio") { 
			
			tamanio.add(elemento);
			
			elemento.addActionListener(new StyledEditorKit.FontSizeAction("cambio_tamanio", tam));
		}
	
		
	}
	
	
	public void abrir() {
		JFileChooser fc = new JFileChooser();
		int seleccion = fc.showOpenDialog(this);
		
		if(seleccion == JFileChooser.APPROVE_OPTION) {
			
			File fichero = fc.getSelectedFile();
			LaminaProcesador.areatexto.setText(fichero.getAbsolutePath());
			
			try (FileReader fr = new FileReader(fichero)){
				String cadena = "";
				int valor = fr.read();
				while (valor != -1) {
					cadena = cadena +(char)valor;
					valor = fr.read();
				}
				LaminaProcesador.areatexto.setText(cadena);
			} catch (IOException e) {
				// TODO: handle exception
				e.printStackTrace();
				
			}
			
		}
		
	}
	
	public void guardar() {
		JFileChooser fc = new JFileChooser();
		int seleccion = fc.showSaveDialog(this);
		
		if(seleccion == JFileChooser.APPROVE_OPTION) {
			
			File fichero = fc.getSelectedFile();

			try (FileWriter fw = new FileWriter(fichero)){
				
				fw.write(LaminaProcesador.areatexto.getText());
				
			} catch (IOException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		JOptionPane.showMessageDialog(null, "Archivo guardado correctamente");
		System.exit(0);
		
	}
	
}

